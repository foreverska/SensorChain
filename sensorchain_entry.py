# SensorChain - Zero code tool to configure Sawtooth for ingestion of simple data
# Copyright (C) 2018  Adam Parker
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, json, time

import cbor

import urllib.request
from urllib.error import HTTPError

from sawtooth_signing.secp256k1_signer import generate_privkey
from sawtooth_sdk.client.encoding import TransactionEncoder
from sawtooth_sdk.client.encoding import BatchEncoder

from sensorchain_handler import VerifyContent
from sensorchain_handler import MakeAddress
from sensorchain_config import ConfigInstance

def LoadEntry(entry):
    if os.path.isfile(entry) != True:
        raise Exception('Config file ' + entry + 'could not be found')
    jsonFile = open(entry)

    return json.load(jsonFile)

def WaitForConfirmation(response):
    jsonResponse = json.loads(response.decode("utf-8"))
    link = jsonResponse['link']
    MAX_CYCLES = 10
    cycle = 0

    status = 'PENDING'
    while status == 'PENDING' and cycle < MAX_CYCLES:
        response = urllib.request.urlopen(link)
        jsonResponse = json.loads(response.read().decode("utf-8"))
        status = jsonResponse['data'][0]['status']
        time.sleep(0.1)
        cycle += 1

    return status

def SendEntry(entry):
    config = ConfigInstance()
    entryDict = LoadEntry(entry)
    entryDict = VerifyContent(config, entryDict)

    private_key = privkey = generate_privkey(privkey_format='bytes')

    encoder = TransactionEncoder(
        private_key,
        payload_encoder = cbor.dumps,
        family_name = config.familyName,
        family_version = config.familyVersion,
        inputs = [''],
        outputs = [''],
        payload_encoding='application/cbor')

    txn = encoder.create(entryDict)

    batcher = BatchEncoder(private_key)
    batch = batcher.create(txn)
    batchBytes = batcher.encode(batch)

    try:
        request = urllib.request.Request(
            'http://127.0.0.1:8080/batches',
            batchBytes,
            method='POST',
            headers={'Content-Type': 'application/octet-stream'})
        response = urllib.request.urlopen(request)

        status = WaitForConfirmation(response.read())

        if status == 'COMMITTED':
            print('Entry successfully comitted')
        elif status == 'INVALID':
            print('Entry marked as invalid')
        else:
            print('Entry likely dropped by validator')

    except HTTPError as e:
        response = e.file
