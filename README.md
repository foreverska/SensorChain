# SensorChain
SensorChain is a zero code tool to configure Sawtooth for ingestion of simple data. Is this a tool that is gravely needed?  Probably not, but it does allow me to practice my Python and more importantly my Sawtooth/Blockchain concepts.  If you have ever had the idea to put a simple sensor on blockchain this might be the tool for you. After getting a sawtooth server running config for this tool should take 10 minutes and then you are up and running.  Just run the client tool to upload new data.

## Config
SensonChain is configured using a single .json file.  It has two sections, config and data model.  The config section currently contains two entries, "family_name" which is the name of your data model and "family_version" which is the version of your data model.

The data model section contains your data model.  Your datamodel must contain the name entry with the type string.  This name identifies the sensor not the data model itself. This can be a serial number (in string format) or a logical identifier. After that all other parameters are up to the author.  The currently supported types are string, int, float and bool.

## Commands
sensorchain_tp is the associated transaction processor.  It has no major rules for verifying an entry only that it has all parts of the data model and all parts are in the correct data type.  Both commands will default to /etc/sensorchain/config.json when looking for config.  Otherwise please specify a different config file.  This is all that is required to run the sensorchain_tp command.  Check -h for other options.

sensorchain_client is the associated client program.  This program loads an entry from a json file specified as a command line option.  The format of the entry must match the datamodel in the config file.  The client will wait for up to 1 second for the status of the batch to change from PENDING.  At time out or when the status changes it will print a message indicating the status of the batch.

## Example
There is an example config in the config folder and an example entry in the entry folder.  The example isn't anything specific but if it helps it could be considered a datamodel for a thermometer at the stockyards.

Pulls with bugfixes or features will be accepted pending testing and codestyle
