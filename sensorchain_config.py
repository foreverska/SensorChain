# SensorChain - Zero code tool to configure Sawtooth for ingestion of simple data
# Copyright (C) 2018  Adam Parker
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json, os

CONFIG_ITEMS = ['family_name', 'family_version']

SUPPORTED_TYPES = {'string': str,
                   'int': int,
                   'float': float,
                   'bool': bool}

def LoadDatamodel(self, contents):
    self._dataModel = {}

    for key, value in contents['datamodel'].items():
        if type(value) != str:
            raise Exception('Datamodel entry', key, "is not of type string")
        if value not in SUPPORTED_TYPES:
            raise Exception('Datamodel entry', key, "has an invalid type")

        self._dataModel[key] = SUPPORTED_TYPES[value]

    return

def LoadItem(name, contents):
    if not name in contents['config']:
        raise Exception("Required paramter ", name, "not in config")
    if type(contents['config'][name]) != str:
        raise Exception("Required parameter ", name, "malformed")

    return contents['config'][name]

def LoadConfig(self, contents):
    self._familyName = LoadItem('family_name', contents)
    self._familyVersion = LoadItem('family_version', contents)
    return

def OpenFile(self, filename):
    if os.path.isfile(filename) != True:
        raise Exception('Config file ' + filename + 'could not be found')
    jsonFile = open(filename)

    try:
        contents = json.load(jsonFile)
        if not 'config' in contents:
            raise Exception()
        if not 'datamodel' in contents:
            raise Exception()
    except Exception as e:
        raise
    except:
        raise Exception("Unknown error loading config file ", "filename")

    return contents

class ScConfig:
    @property
    def familyName(self):
        return self._familyName

    @property
    def familyVersion(self):
        return self._familyVersion

    @property
    def datamodel(self):
        return self._dataModel

    def LoadConfigFile(self, filename):
        contents = OpenFile(self, filename)
        LoadConfig(self, contents)
        LoadDatamodel(self, contents)

SensorConfig = ScConfig()

def ConfigInstance():
    return SensorConfig
