# SensorChain - Zero code tool to configure Sawtooth for ingestion of simple data
# Copyright (C) 2018  Adam Parker
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
import hashlib

import cbor

from sawtooth_sdk.processor.context import StateEntry
from sawtooth_sdk.processor.exceptions import InvalidTransaction
from sawtooth_sdk.processor.exceptions import InternalError

from sensorchain_config import ConfigInstance

def MakeAddressPrefix():
    nameHash = hashlib.sha512(ConfigInstance().familyName.encode('utf-8'))
    return nameHash.hexdigest()[0:6]

def MakeAddress(name):
    return MakeAddressPrefix() + hashlib.sha512(
        name.encode('utf-8')).hexdigest()[-64:]

def UpdateState(unpacked, state):
    for key in unpacked:
        state[key] = unpacked[key]

    return state

def VerifyContent(config, content):
    unpacked = {}

    if len(content) != len(config.datamodel):
        raise Exception('Size of data different than datamodel.')

    for key, value in content.items():
        if key not in config.datamodel:
            raise Exception(key, 'not in datamodel')
        if type(value) != config.datamodel[key]:
            raise Exception(key, 'has the wrong datatype')
        unpacked[key] = value

    return unpacked


def UnpackTransaction(config, transaction):
    try:
        content = cbor.loads(transaction.payload)
        unpacked = VerifyContent(config, content)

    except:
        raise InvalidTransaction('Invalid payload serialization')

    return unpacked

def SetStateData(name, state, stateStore):
    address = MakeAddress(name)

    encoded = cbor.dumps(state)

    addresses = stateStore.set([StateEntry(address=address, data=encoded)])

    if not addresses:
        raise InternalError('State error')

def GetStateData(name, stateStore):
    address = MakeAddress(name)

    state_entries = stateStore.get([address])

    try:
        return cbor.loads(state_entries[0].data)
    except IndexError:
        return {}
    except:
        raise InternalError('Failed to load state data')


class SensorChainHandler:
    _config = ConfigInstance()

    @property
    def family_name(self):
        return self._config.familyName

    @property
    def family_versions(self):
        return [self._config.familyVersion]

    @property
    def encodings(self):
        return ['application/cbor']

    @property
    def namespaces(self):
        return [MakeAddressPrefix()]

    def apply(self, transaction, stateStore):
        unpacked = UnpackTransaction(self._config, transaction)

        state = GetStateData(unpacked['name'], stateStore)

        updatedState = UpdateState(unpacked, state)

        SetStateData(unpacked['name'], updatedState, stateStore)
