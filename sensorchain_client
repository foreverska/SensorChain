#! /usr/bin/python3

# SensorChain - Zero code tool to configure Sawtooth for ingestion of simple data
# Copyright (C) 2018  Adam Parker
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, argparse

from sawtooth_sdk.processor.core import TransactionProcessor
from sawtooth_sdk.client.log import init_console_logging
from sawtooth_sdk.client.log import log_configuration
from sawtooth_sdk.client.config import get_log_config
from sawtooth_sdk.client.config import get_log_dir

from sensorchain_entry import SendEntry
from sensorchain_config import ConfigInstance

def ParseArguments(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('config',
                        help='config file',
                        default='/etc/sensorchain/config.json')
    parser.add_argument('entry',
                        help='Entry to load to the chain')

    return parser.parse_args(args)

def main(args):
    config = ConfigInstance()
    opts = ParseArguments(args)
    config.LoadConfigFile(opts.config)

    processor = None
    try:

        SendEntry(opts.entry)

    except KeyboardInterrupt:
        pass
    except Exception as e:  # pylint: disable=broad-except
        print("Error: {}".format(e), file=sys.stderr)
    finally:
        if processor is not None:
            processor.stop()


main(sys.argv[1:])
